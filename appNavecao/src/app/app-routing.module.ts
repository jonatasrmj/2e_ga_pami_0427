import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'adm-login',
    pathMatch: 'full'
  },
  {
    path: 'cad-autor',
    loadChildren: () => import('./admin/cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('./admin/cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  },
  {
    path: 'cad-livro',
    loadChildren: () => import('./admin/cad-livro/cad-livro.module').then( m => m.CadLivroPageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./admin/cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'adm-login',
    loadChildren: () => import('./admin/adm-login/adm-login.module').then( m => m.AdmLoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./admin/listagem/usuarios/usuarios.module').then( m => m.UsuariosPageModule)
  },
  {
    path: 'livros',
    loadChildren: () => import('./admin/listagem/livros/livros.module').then( m => m.LivrosPageModule)
  },
  {
    path: 'autores',
    loadChildren: () => import('./admin/listagem/autores/autores.module').then( m => m.AutoresPageModule)
  },
  {
    path: 'editoras',
    loadChildren: () => import('./admin/listagem/editoras/editoras.module').then( m => m.EditorasPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
