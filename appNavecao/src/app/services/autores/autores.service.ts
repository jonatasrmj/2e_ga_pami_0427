import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_D = "https://3000-jonatasrmj-2egaapi0810-ffk9kl62nb5.ws-us79.gitpod.io/"
  private readonly URL_J = "https://3000-jonatasrmj-2egaapi0810-o7wouxydnte.ws-us74.gitpod.io/"
  private readonly URL = this.URL_D

  constructor(
    private http: HttpClient
  ) { }

  cadastrarAutor(autor: Autor): Observable<any> {
    return this.http.post<any>(`${this.URL}autor`, autor)
  }

  listarAutores(): Observable<any>{
    return this.http.get<any>(`${this.URL}autores`)
  }
}
