import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Editora } from 'src/app/models/Editora';

@Injectable({
  providedIn: 'root'
})
export class EditorasService {
  private readonly URL_D = "https://3000-jonatasrmj-2egaapi0810-ffk9kl62nb5.ws-us79.gitpod.io/"
  private readonly URL_J = "https://3000-jonatasrmj-2egaapi0810-o7wouxydnte.ws-us74.gitpod.io/"
  private readonly URL = this.URL_D

  constructor(
    private http: HttpClient
  ) { }

  cadastrarEditora(editora: Editora): Observable<any> {
    return this.http.post<any>(`${this.URL}editora`, editora)
  }
  listarEditoras(): Observable<any> {
    return this.http.get<any>(`${this.URL}editoras`)
  }
}