import { Component, OnInit } from '@angular/core';
import { Livro } from 'src/app/models/Livro';
import { LivrosService } from 'src/app/services/livros/livros.service';

@Component({
  selector: 'app-cad-livro',
  templateUrl: './cad-livro.page.html',
  styleUrls: ['./cad-livro.page.scss'],
})
export class CadLivroPage implements OnInit {
  livro: Livro
  constructor(
    private livrosService: LivrosService
  ) { 
    this.livro = new Livro()
  }

  ngOnInit() {}
    cadastrarLivro(): void{
      this.livrosService.cadastrarLivro(this.livro).subscribe(
        {
          next:(dados)=>{
            console.log(this.livro)
            console.log(dados)
          },
          error:(error)=>{
            console.log(this.livro)
            console.error(error)
          }
        }
      )
    }

}
