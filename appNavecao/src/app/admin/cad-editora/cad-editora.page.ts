import { Component, OnInit } from '@angular/core';
import { Editora } from 'src/app/models/Editora';
import { EditorasService } from 'src/app/services/editoras/editoras.service';

@Component({
  selector: 'app-cad-editora',
  templateUrl: './cad-editora.page.html',
  styleUrls: ['./cad-editora.page.scss'],
})
export class CadEditoraPage implements OnInit {
  editora: Editora
  constructor(
    private editorasService: EditorasService
  ) { 
    this.editora = new Editora()
  }

  ngOnInit() {}
  cadastrarEditora(): void{
    this.editorasService.cadastrarEditora(this.editora).subscribe(
      {
        next:(dados)=>{
          console.log(this.editora)
          console.log(dados)
        },
        error:(error)=>{
          console.log(this.editora)
          console.error(error)
        }
      }
    )
  }

}
