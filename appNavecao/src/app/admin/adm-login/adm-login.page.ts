import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-adm-login',
  templateUrl: './adm-login.page.html',
  styleUrls: ['./adm-login.page.scss'],
})
export class AdmLoginPage implements OnInit {
  usuario: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.usuario = new Usuario()
  }

  ngOnInit() {
  }

  logar(): void {
    console.log(this.usuario)

    this.usuariosService.logar(this.usuario)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
