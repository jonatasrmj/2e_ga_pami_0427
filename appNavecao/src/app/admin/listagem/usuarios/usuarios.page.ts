import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';

import { UsuariosService } from '../../../services/usuarios/usuarios.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {

  constructor(
    private usuariosService: UsuariosService
  ) { }
  usuariosCadastrados : Usuario [] = []
  ngOnInit() {
    this.usuariosService.listarUsuarios().subscribe(
      {
        next:(response) => {
          this.usuariosCadastrados=response.result
          console.log(response)
        },
        error:(error) => {
          console.log(error)
        }
      }
      
    )
  }

}
