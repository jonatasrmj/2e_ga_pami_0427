import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';

import { AutoresService } from '../../../services/autores/autores.service';

@Component({
  selector: 'app-autor',
  templateUrl: './autores.page.html',
  styleUrls: ['./autores.page.scss'],
})
export class AutoresPage implements OnInit {

  constructor(
    private autoresService: AutoresService
  ) { }
  autoresCadastrados : Autor [] = []
  ngOnInit() {
    this.autoresService.listarAutores().subscribe(
      {
        next:(response) => {
          this.autoresCadastrados=response.result
          console.log(response)
        },
        error:(error) => {
          console.log(error)
        }
      }
      
    )
  }

}
