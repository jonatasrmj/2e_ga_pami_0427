import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutoresPage } from './autores.page';

const routes: Routes = [
  {
    path: '',
    component: AutoresPage
  },
  {
    path: 'cad-autor',
    loadChildren: () => import('src/app/admin/cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AutoresPageRoutingModule {}
