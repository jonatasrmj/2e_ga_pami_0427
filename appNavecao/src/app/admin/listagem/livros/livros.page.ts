import { Component, OnInit } from '@angular/core';
import { Livro } from 'src/app/models/Livro';

import { LivrosService } from '../../../services/livros/livros.service';

@Component({
  selector: 'app-livros',
  templateUrl: './livros.page.html',
  styleUrls: ['./livros.page.scss'],
})
export class LivrosPage implements OnInit {

  constructor(
    private livrosService: LivrosService
  ) { }
  livrosCadastrados : Livro [] = []
  ngOnInit() {
    this.livrosService.listarLivros().subscribe(
      {
        next:(response) => {
          this.livrosCadastrados=response.result
          console.log(response)
        },
        error:(error) => {
          console.log(error)
        }
      }
      
    )
  }

}
