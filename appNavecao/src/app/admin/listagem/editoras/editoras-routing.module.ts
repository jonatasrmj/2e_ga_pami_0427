import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditorasPage } from './editoras.page';

const routes: Routes = [
  {
    path: '',
    component: EditorasPage
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('src/app/admin/cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditorasPageRoutingModule {}
