import { Component, OnInit } from '@angular/core';
import { Editora } from 'src/app/models/Editora';

import { EditorasService } from '../../../services/editoras/editoras.service';

@Component({
  selector: 'app-editora',
  templateUrl: './editoras.page.html',
  styleUrls: ['./editoras.page.scss'],
})
export class EditorasPage implements OnInit {

  constructor(
    private editorasService: EditorasService
  ) { }
  editorasCadastrados : Editora [] = []
  ngOnInit() {
    this.editorasService.listarEditoras().subscribe(
      {
        next:(response) => {
          this.editorasCadastrados=response.result
          console.log(response)
        },
        error:(error) => {
          console.log(error)
        }
      }
      
    )
  }

}
