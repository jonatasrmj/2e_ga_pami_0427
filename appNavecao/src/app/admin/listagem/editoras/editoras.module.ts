import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditorasPageRoutingModule } from './editoras-routing.module';

import { EditorasPage } from './editoras.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditorasPageRoutingModule
  ],
  declarations: [EditorasPage]
})
export class EditorasPageModule {}
