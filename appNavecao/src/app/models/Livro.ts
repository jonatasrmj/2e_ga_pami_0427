export class Livro{
    id: number 
    titulo: string
    paginas: number
    resumo: string
    preco: number 
    idioma: string 
    edicao: number 
    id_autor: number 
    id_editora: number 
    data_publicacao: Date 

    constructor()
    {
        this.id=0,
        this.titulo="",
        this.paginas=0,
        this.resumo="",
        this.preco=0,
        this.idioma="",
        this.edicao=0,
        this.id_autor=0,
        this.id_editora=0,
        this.data_publicacao= new Date()
    }
}
