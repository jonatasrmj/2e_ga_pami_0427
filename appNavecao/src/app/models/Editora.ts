export class Editora{
    id: number = 0
    razao_social: string = ""
    cnpj: string = ""
    ie: string = ""

    constructor()
    {
        this.id=0,
        this.razao_social="",
        this.cnpj="",
        this.ie=""
    }
}